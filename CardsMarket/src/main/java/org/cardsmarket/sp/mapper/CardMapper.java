package org.cardsmarket.sp.mapper;

import org.cardsmarket.sp.dto.CardDTO;
import org.cardsmarket.sp.model.Card;
import org.springframework.stereotype.Component;

@Component
public class CardMapper {
    public CardDTO toDTO(Card card) {
        return new CardDTO(card.getId_card(), card.getName(), card.getPrice(), card.getFamily_name(), card.getDescription(), card.getAttack(),
                card.getDefense(), card.getEnergy(), card.getHp(), card.getOwner().getPseudo(), card.getImgUrl());
    }

    public Card toModel(CardDTO card) {
        return new Card(card.getId_card(), card.getName(), card.getPrice(), card.getFamily_name(), card.getDescription(), card.getAttack(),
                card.getDefense(), card.getEnergy(), card.getHp(), card.getImg_url());
    }
}

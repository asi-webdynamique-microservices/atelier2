package org.cardsmarket.sp.mapper;

import org.cardsmarket.sp.dto.TransactionDTO;
import org.cardsmarket.sp.model.Transaction;
import org.cardsmarket.sp.service.CardService;
import org.cardsmarket.sp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TransactionMapper {

    @Autowired
    UserService userService;
    @Autowired
    CardService cardService;
    public TransactionDTO toDTO(Transaction transaction){
        TransactionDTO transactionDTO =  new TransactionDTO(transaction.getId_transaction(), transaction.getUserSeller().getPseudo(), transaction.getCard().getId_card(), transaction.getDescription());
        if(transaction.getUserBuyer() != null){
            transactionDTO.setPseudo_buyer(transaction.getUserBuyer().getPseudo());
        }
        return transactionDTO;
    }
    public Transaction toModel(TransactionDTO transactionDTO){
        return new Transaction(userService.findUser(transactionDTO.getPseudo_buyer()),
                userService.findUser(transactionDTO.getPseudo_seller()), cardService.getCardbyId(transactionDTO.getId_card()),
                transactionDTO.getDescription());
    }
}

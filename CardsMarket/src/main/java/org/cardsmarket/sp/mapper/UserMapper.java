package org.cardsmarket.sp.mapper;

import org.cardsmarket.sp.dto.UserDTO;
import org.cardsmarket.sp.model.User;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {
    public UserDTO toDTO(User user){
        return new UserDTO(user.getPseudo(), user.getName(), user.getSurname(), user.getMail(), user.getWallet());
    }
    public User toModel(UserDTO userDTO){
        return new User(userDTO.getPseudo(), userDTO.getName(), userDTO.getSurname(), userDTO.getMail(), userDTO.getWallet());
    }
}

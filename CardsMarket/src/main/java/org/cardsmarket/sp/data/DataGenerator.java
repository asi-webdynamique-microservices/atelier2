package org.cardsmarket.sp.data;

import org.cardsmarket.sp.dao.CardDAO;
import org.cardsmarket.sp.dao.TransactionDAO;
import org.cardsmarket.sp.dao.UserDAO;
import org.cardsmarket.sp.model.Card;
import org.cardsmarket.sp.model.Transaction;
import org.cardsmarket.sp.model.User;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class DataGenerator implements ApplicationRunner {

    private final UserDAO userDAO;
    private final TransactionDAO transactionDAO;
    private final CardDAO cardDAO;

    public DataGenerator(UserDAO userDAO, TransactionDAO transactionDAO, CardDAO cardDAO) {
        this.userDAO = userDAO;
        this.transactionDAO = transactionDAO;
        this.cardDAO = cardDAO;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        if (this.userDAO.findAll().size() != 0) {
            System.out.println("Pas de traitement, déja des données en bdd");
            return;
        }

        User user2 = new User();
        user2.setPseudo("MALLORY");
        user2.setWallet(1000);
        this.userDAO.save(user2);

        User user3 = new User();
        user3.setPseudo("CardsMarket");
        user3.setWallet(1000000);
        this.userDAO.save(user3);

        Card card = new Card();
        card.setOwner(user2);
        card.setName("Chat domestique");
        card.setImgUrl("https://cdn.futura-sciences.com/buildsv6/images/wide1920/0/0/d/00dd1479a5_108485_chat-domestique.jpg");
        card.setAttack(10);
        card.setDefense(5);
        card.setFamily_name("Félin");
        card.setDescription("Ceci est un chat domestique");
        card.setHp(100);
        card.setEnergy(300);
        card.setPrice(100);
        this.cardDAO.save(card);

        Card card2 = new Card();
        card2.setOwner(user2);
        card2.setName("Chien domestique");
        card2.setImgUrl("https://www.mondelibre.org/wp-content/uploads/chien-heureux.jpg");
        card2.setAttack(15);
        card2.setDefense(0);
        card2.setFamily_name("Canin");
        card2.setDescription("Ceci est un chien domestique");
        card2.setHp(200);
        card2.setEnergy(200);
        card2.setPrice(150);
        this.cardDAO.save(card2);
    }
}

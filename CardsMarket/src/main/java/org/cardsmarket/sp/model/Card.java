package org.cardsmarket.sp.model;

import javax.persistence.*;

@Entity
public class Card {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Integer id_card;
    private String name;
    private int price;
    private String family_name;
    private String description;
    private int attack;
    private int defense;
    private int energy;
    private int hp;
    @ManyToOne
    @JoinColumn(name="pseudo_owner")
    private User owner;
    private String imgUrl;

    public Card() {
    }

    public Card(Integer id_card, String name, int price, String family_name, String description, int attack, int defense, int energy, int hp, String imgUrl) {
        this.id_card = id_card;
        this.name = name;
        this.price = price;
        this.family_name = family_name;
        this.description = description;
        this.attack = attack;
        this.defense = defense;
        this.energy = energy;
        this.hp = hp;
        this.imgUrl = imgUrl;
    }

    public Integer getId_card() {
        return id_card;
    }

    public void setId_card(Integer id_card) {
        this.id_card = id_card;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getFamily_name() {
        return family_name;
    }

    public void setFamily_name(String family_name) {
        this.family_name = family_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getAttack() {
        return attack;
    }

    public void setAttack(int attack) {
        this.attack = attack;
    }

    public int getDefense() {
        return defense;
    }

    public void setDefense(int defense) {
        this.defense = defense;
    }

    public int getEnergy() {
        return energy;
    }

    public void setEnergy(int energy) {
        this.energy = energy;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getName() {
        return name;
    }

    public Card setName(String name) {
        this.name = name;
        return this;
    }
}
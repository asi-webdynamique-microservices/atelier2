package org.cardsmarket.sp.model;
import javax.persistence.*;


@Entity
public class Transaction {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id_transaction;
    @ManyToOne
    @JoinColumn(name = "pseudo_buyer")
    private User userBuyer;
    @ManyToOne
    @JoinColumn(name = "pseudo_seller")
    private User userSeller;
    @ManyToOne
    @JoinColumn(name="id_card")
    private Card card;
    private String description;

    public Transaction(User userBuyer, User userSeller, Card card, String description) {
        this.userBuyer = userBuyer;
        this.userSeller = userSeller;
        this.card = card;
        this.description = description;
    }

    public Transaction() {
    }

    public int getId_transaction() {
        return id_transaction;
    }

    public void setId_transaction(int id_transaction) {
        this.id_transaction = id_transaction;
    }

    public User getUserBuyer() {
        return userBuyer;
    }

    public void setUserBuyer(User userBuyer) {
        this.userBuyer = userBuyer;
    }

    public User getUserSeller() {
        return userSeller;
    }

    public void setUserSeller(User userSeller) {
        this.userSeller = userSeller;
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
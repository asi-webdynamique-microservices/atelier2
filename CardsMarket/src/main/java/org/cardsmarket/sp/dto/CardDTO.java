package org.cardsmarket.sp.dto;

public class CardDTO {

    private Integer id_card;
    private int price;
    private String name;
    private String family_name;
    private String description;
    private int attack;
    private int defense;
    private int energy;
    private int hp;
    private String pseudo_owner;
    private String img_url;

    public CardDTO(Integer id_card, String name, int price, String family_name, String description, int attack, int defense, int energy, int hp, String pseudo_owner, String img_url) {
        this.id_card = id_card;
        this.price = price;
        this.name = name;
        this.family_name = family_name;
        this.description = description;
        this.attack = attack;
        this.defense = defense;
        this.energy = energy;
        this.hp = hp;
        this.pseudo_owner = pseudo_owner;
        this.img_url = img_url;
    }

    public Integer getId_card() {
        return id_card;
    }

    public void setId_card(Integer id_card) {
        this.id_card = id_card;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getFamily_name() {
        return family_name;
    }

    public void setFamily_name(String family_name) {
        this.family_name = family_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getAttack() {
        return attack;
    }

    public void setAttack(int attack) {
        this.attack = attack;
    }

    public int getDefense() {
        return defense;
    }

    public void setDefense(int defense) {
        this.defense = defense;
    }

    public int getEnergy() {
        return energy;
    }

    public void setEnergy(int energy) {
        this.energy = energy;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public String getPseudo_owner() {
        return pseudo_owner;
    }

    public void setPseudo_owner(String pseudo_owner) {
        this.pseudo_owner = pseudo_owner;
    }

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    public String getName() {
        return name;
    }

    public CardDTO setName(String name) {
        this.name = name;
        return this;
    }
}

package org.cardsmarket.sp.dto;

public class TransactionDTO {
    private int id_transaction;
    private String pseudo_seller;
    private String pseudo_buyer;
    private int id_card;
    private String description;

    public TransactionDTO() {
    }

    public TransactionDTO(int id_transaction, String pseudo_seller, int id_card, String description) {
        this.id_transaction = id_transaction;
        this.pseudo_seller = pseudo_seller;
        this.id_card = id_card;
        this.description = description;
    }

    public TransactionDTO(int id_transaction, String description, String pseudo_seller) {
        this.id_transaction = id_transaction;
        this.description = description;
    }

    public int getId_transaction() {
        return id_transaction;
    }

    public TransactionDTO setId_transaction(int id_transaction) {
        this.id_transaction = id_transaction;
        return this;
    }

    public String getPseudo_seller() {
        return pseudo_seller;
    }

    public TransactionDTO setPseudo_seller(String pseudo_seller) {
        this.pseudo_seller = pseudo_seller;
        return this;
    }

    public String getPseudo_buyer() {
        return pseudo_buyer;
    }

    public TransactionDTO setPseudo_buyer(String pseudo_buyer) {
        this.pseudo_buyer = pseudo_buyer;
        return this;
    }

    public int getId_card() {
        return id_card;
    }

    public TransactionDTO setId_card(int id_card) {
        this.id_card = id_card;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public TransactionDTO setDescription(String description) {
        this.description = description;
        return this;
    }
}

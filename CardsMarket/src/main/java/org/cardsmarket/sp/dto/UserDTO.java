package org.cardsmarket.sp.dto;

public class UserDTO {
    private String pseudo;
    private String name;
    private String surname;
    private String mail;
    private int wallet;

    public UserDTO(String pseudo, String name, String surname, String mail, int wallet) {
        this.pseudo = pseudo;
        this.name = name;
        this.surname = surname;
        this.mail = mail;
        this.wallet = wallet;
    }

    public String getPseudo() {
        return pseudo;
    }

    public UserDTO setPseudo(String pseudo) {
        this.pseudo = pseudo;
        return this;
    }

    public String getName() {
        return name;
    }

    public UserDTO setName(String name) {
        this.name = name;
        return this;
    }

    public String getSurname() {
        return surname;
    }

    public UserDTO setSurname(String surname) {
        this.surname = surname;
        return this;
    }

    public String getMail() {
        return mail;
    }

    public UserDTO setMail(String mail) {
        this.mail = mail;
        return this;
    }

    public int getWallet() {
        return wallet;
    }

    public UserDTO setWallet(int wallet) {
        this.wallet = wallet;
        return this;
    }
}

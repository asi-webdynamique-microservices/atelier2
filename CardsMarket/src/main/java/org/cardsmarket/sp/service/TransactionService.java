package org.cardsmarket.sp.service;

import org.cardsmarket.sp.dao.TransactionDAO;
import org.cardsmarket.sp.dao.UserDAO;
import org.cardsmarket.sp.dto.TransactionDTO;
import org.cardsmarket.sp.model.Card;
import org.cardsmarket.sp.model.Transaction;
import org.cardsmarket.sp.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TransactionService {

    @Autowired
    TransactionDAO transactionDao;

    @Autowired
    UserService userService;
    @Autowired
    CardService cardService;

    public Transaction addTransaction(String pseudoUserSeller, int id_card, String description){
        Transaction transaction =  new Transaction(null, userService.findUser(pseudoUserSeller),
                cardService.getCardbyId(id_card), description);
        Card card =  cardService.getCardbyId(id_card);
        cardService.updateCard(card, "CardsMarket");
        return transactionDao.save(transaction);
    }

    public List<Transaction> getAllTransactions() {
        return transactionDao.findAll();
    }

    public List<Transaction> getAllTransactionWithoutBuyer (){
        return transactionDao.findByUserBuyer(null);
    }

    public Transaction getTransaction (int id) {
        Optional<Transaction> transactionOptional = transactionDao.findById(id);
        if (transactionOptional.isPresent())
            return transactionOptional.get();
        else
            return null;
    }

    public List<Transaction> getTransactionByUserBuyer (User user){
        return transactionDao.findByUserBuyer(user);
    }


    public List<Transaction> getTransactionByUserSeller (User user){
        return transactionDao.findByUserSeller(user);
    }

    public List<Transaction> getTransactionByCard (Card card){
        return transactionDao.findByCard(card);
    }

    public Transaction updateTransaction (Transaction transactionToUpdate){
        return transactionDao.save(transactionToUpdate);
    }

    public Transaction setBuyer (int idTransaction, String pseudoBuyer){
        Transaction transaction = getTransaction(idTransaction);
        transaction.setUserBuyer(userService.findUser(pseudoBuyer));
        return transaction;
    }

    public Transaction setSeller (int idTransaction, String pseudoSeller){
        Transaction transaction = getTransaction(idTransaction);
        transaction.setUserSeller(userService.findUser(pseudoSeller));
        return transaction;
    }
}

package org.cardsmarket.sp.service;

import org.cardsmarket.sp.dto.CardDTO;
import org.cardsmarket.sp.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.cardsmarket.sp.model.Card;
import org.cardsmarket.sp.dao.CardDAO;

import java.util.List;
import java.util.Optional;


@Service
public class CardService {

    @Autowired
    CardDAO cardDAO;
    @Autowired
    UserService userService;

    public Card getCardbyId(Integer id) {
        Optional<Card> card = cardDAO.findById(id);//findById_Card
            return card.get();
    }

    public Card addCard(Card cardToAdd, String pseudo) {
        User user = userService.findUser(pseudo);
        cardToAdd.setOwner(user);
        return cardDAO.save(cardToAdd);
    }

    public List<Card> getCardsList() {
        List<Card> carte = cardDAO.findAll();
        return carte;
    }

    public List<Card> getCardsByPseudoOwner(String pseudo) {
        User user = userService.findUser(pseudo);
        return cardDAO.findByOwner(user);
    }

    public Card updateCard(Card cardToAdd, String pseudo) {
        User user = userService.findUser(pseudo);
        cardToAdd.setOwner(user);
        return cardDAO.save(cardToAdd);
    }
}

package org.cardsmarket.sp.service;

import org.cardsmarket.sp.dao.UserDAO;
import org.cardsmarket.sp.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserDAO userDAO;

    public User addUser(User userToAdd){
        return userDAO.save(userToAdd);
    }

    public User findUser(String pseudo){
        return userDAO.findById(pseudo)
                .orElseThrow(() -> new RuntimeException("Pas de user pour le login " + pseudo));
    }

    public void deleteUser(String pseudo){
        try{
         userDAO.deleteById(pseudo);
        }catch (Exception e){
            throw new RuntimeException("Impossible de supprimer le user pour le login " + pseudo);
        }
    }
}

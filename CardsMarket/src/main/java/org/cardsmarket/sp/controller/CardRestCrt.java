package org.cardsmarket.sp.controller;

import org.cardsmarket.sp.dto.CardDTO;
import org.cardsmarket.sp.dto.UserDTO;
import org.cardsmarket.sp.mapper.CardMapper;
import org.cardsmarket.sp.mapper.UserMapper;
import org.cardsmarket.sp.model.Card;
import org.cardsmarket.sp.model.User;
import org.cardsmarket.sp.service.CardService;
import org.cardsmarket.sp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class CardRestCrt {
    @Autowired
    CardService cardService;
    @Autowired
    CardMapper cardMapper;

   @RequestMapping(method= RequestMethod.GET,value="/cards/{id}")
    public CardDTO getCardbyId(@PathVariable("id") Integer id) {
        Card card = cardService.getCardbyId(id);
        return cardMapper.toDTO(card);
    }

    @RequestMapping(method=RequestMethod.GET,value="/cards")
    public List<CardDTO> getCardsList() {
        List<CardDTO> cardDTO = new ArrayList<CardDTO>();
        List<Card> card = cardService.getCardsList();
        for (Card c : card){
            cardDTO.add(cardMapper.toDTO(c));
        }
        return cardDTO;
    }

    @RequestMapping(method=RequestMethod.POST,value="/cards")
    public CardDTO addCard(@RequestBody CardDTO cardDTOToAdd) {
        return cardMapper.toDTO(cardService.addCard(cardMapper.toModel(cardDTOToAdd), cardDTOToAdd.getPseudo_owner()));
    }

    @RequestMapping(method=RequestMethod.GET,value="/cards/user")
    public List<CardDTO> getCardsOfaUser(@RequestParam("pseudo") String pseudo){
        List<CardDTO> cardsDTO = new ArrayList<CardDTO>();
        for (Card c : cardService.getCardsByPseudoOwner(pseudo)){
            cardsDTO.add(cardMapper.toDTO(c));
        }
        return cardsDTO;
    }

    @RequestMapping(method=RequestMethod.PUT,value="/cards")
    public CardDTO updateCard(@RequestBody CardDTO cardDTOToAdd) {
        return cardMapper.toDTO(cardService.updateCard(cardMapper.toModel(cardDTOToAdd), cardDTOToAdd.getPseudo_owner()));
    }
    

}

package org.cardsmarket.sp.controller;


import org.cardsmarket.sp.dto.UserDTO;
import org.cardsmarket.sp.mapper.UserMapper;
import org.cardsmarket.sp.model.User;
import org.cardsmarket.sp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class UserRestCrt {

    @Autowired
    UserService userService;

    @Autowired
    UserMapper userMapper;

    // Create User
    @RequestMapping(method = RequestMethod.POST, value = "/users")
    public UserDTO addUser(@RequestBody UserDTO userDTO){
        return userMapper.toDTO(userService.addUser(userMapper.toModel(userDTO)));
    }

    @GetMapping("/users")
    public UserDTO getUserByPseudo(@RequestParam(name = "pseudo") String pseudo){
        return userMapper.toDTO(userService.findUser(pseudo));
    }

    @DeleteMapping("/users")
    public void deleteUserByPseudo(@RequestParam(name = "pseudo") String pseudo){
        userService.deleteUser(pseudo);
    }
}

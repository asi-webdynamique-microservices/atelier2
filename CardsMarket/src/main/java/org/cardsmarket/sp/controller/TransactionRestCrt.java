package org.cardsmarket.sp.controller;

import org.cardsmarket.sp.dto.CardDTO;
import org.cardsmarket.sp.dto.TransactionDTO;
import org.cardsmarket.sp.mapper.CardMapper;
import org.cardsmarket.sp.mapper.TransactionMapper;
import org.cardsmarket.sp.model.Card;
import org.cardsmarket.sp.model.Transaction;
import org.cardsmarket.sp.service.CardService;
import org.cardsmarket.sp.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class TransactionRestCrt {

    @Autowired
    CardService cardService;
    @Autowired
    CardMapper userService;
    @Autowired
    CardMapper cardMapper;
    @Autowired
    CardMapper userMapper;
    @Autowired
    private TransactionService transactionService;
    @Autowired
    private TransactionMapper transactionMapper;

    //Sell a card
    @RequestMapping(method= RequestMethod.POST,value="/transactions")
    public TransactionDTO sellCard(@RequestBody TransactionDTO transaction){
        return transactionMapper.toDTO(transactionService.addTransaction(transaction.getPseudo_seller(),
                transaction.getId_card(), transaction.getDescription()));
    }

    //Buy a card
    @RequestMapping(method= RequestMethod.PUT,value="/transactions/{idTransaction}/buyer/{pseudoUser}")
    public TransactionDTO buyCard(@PathVariable int idTransaction, @PathVariable String pseudoUser){
        return transactionMapper.toDTO(transactionService.setBuyer(idTransaction, pseudoUser));
    }

    //Get all transactions
    @RequestMapping(method=RequestMethod.GET,value="/transactions")
    public List<TransactionDTO> getAllTransactions(){
        List<TransactionDTO> allTransactionDTO = new ArrayList<TransactionDTO>();
        for (Transaction t : transactionService.getAllTransactions()){
            allTransactionDTO.add(transactionMapper.toDTO(t));
        }
        return allTransactionDTO;
    }
}

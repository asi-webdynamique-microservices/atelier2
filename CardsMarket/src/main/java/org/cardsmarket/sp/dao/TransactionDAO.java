package org.cardsmarket.sp.dao;

import org.cardsmarket.sp.model.Card;
import org.cardsmarket.sp.model.Transaction;
import org.cardsmarket.sp.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TransactionDAO extends JpaRepository<Transaction, Integer> {
    List<Transaction> findByUserBuyer(User userBuyer);
    List<Transaction> findByUserSeller(User userSeller);
    List<Transaction> findByCard(Card card);
}

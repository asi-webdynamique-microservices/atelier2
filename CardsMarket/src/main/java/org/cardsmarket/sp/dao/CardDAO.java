package org.cardsmarket.sp.dao;

import org.cardsmarket.sp.model.Card;
import org.cardsmarket.sp.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;

public interface CardDAO extends JpaRepository<Card, Integer> {
        List<Card> findByOwner(User user);
}

package org.cardsmarket.sp.dao;

import org.cardsmarket.sp.model.Transaction;
import org.cardsmarket.sp.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserDAO extends JpaRepository<User, String> {

}
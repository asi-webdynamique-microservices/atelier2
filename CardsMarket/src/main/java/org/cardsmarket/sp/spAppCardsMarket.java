package org.cardsmarket.sp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class spAppCardsMarket {
    public static void main(String[] args) {
        SpringApplication.run(spAppCardsMarket.class,args);
    }
}
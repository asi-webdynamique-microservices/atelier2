let userLoged = null
let myWallet = null

getMyData()
getMyCard()

function getMyData(){
    fetch('http://localhost:8989/users?pseudo=MALLORY').then(response => response.json())
    .then(data => {
        console.log(data)
        myUserName = document.getElementById('userNameId')
        myWallet = document.getElementById('myWallet')
        myUserName.innerHTML = data.pseudo
        myWallet.innerHTML = data.wallet
        userLoged = data.pseudo
        myWallet = data.wallet
    })
}

function getMyCard(){
    fetch('http://localhost:8989/cards/user?pseudo=MALLORY').then(response => response.json())
    .then(data => {

        for (const row of document.getElementsByClassName("cardRow")) {
            row.innerHTML = ""
        }
    
        let cardList = data
        let template = document.querySelector("#row");
    
        for(const card of cardList){
            let clone = document.importNode(template.content, true);
    
            newContent = clone.firstElementChild.innerHTML
                        .replace(/{{family_name}}/g, card.family_name)
                        .replace(/{{img_src}}/g, card.img_url)
                        .replace(/{{name}}/g, card.name)
                        .replace(/{{description}}/g, card.description)
                        .replace(/{{hp}}/g, card.hp)
                        .replace(/{{energy}}/g, card.energy)
                        .replace(/{{attack}}/g, card.attack)
                        .replace(/{{defense}}/g, card.defense)
                        .replace(/{{price}}/g, card.price)
                        .replace(/{{button_card}}/g, "sell"+card.id_card);
    
            clone.firstElementChild.innerHTML = newContent;
    
            let cardContainer = document.querySelector("#tableContent");
            cardContainer.appendChild(clone);
            
            document.getElementById("sell"+card.id_card).addEventListener('click', function (event) {
                const data = {
                    "description": "",
                    "pseudo_buyer": "",
                    "pseudo_seller": userLoged,
                    "id_card": card.id_card
                };
                fetch('http://localhost:8989/transactions', {
                    method: 'POST',
                    headers: {'Content-Type': 'application/json',},
                    body: JSON.stringify(data),
                })
                .then(response => response.json())
                .then(data => {
                    alert("Vous avez mis en vente la carte n°" + data.id_card);
                    getMyCard()
                })
                .catch((error) => {
                    console.error('Error:', error);
                });
            });
        }
    
    })
}






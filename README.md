# Atelier2

## Pour lancer le projet

Dans un terminal :

cd ./Database && docker-compose up

Ouvrir le projet CardsMarket avec Intellij et lancer un run.

Consulter [http://localhost:8989/](http://localhost:8989/) pour l'interface web

Consulter [http://localhost:8080/](http://localhost:8080/) pour la bdd :

- System : MySQL
- Server : db
- Username : root
- Password : password
- Database : CardsMarket



## Ce qui marche

L'ensemble du front a été créé sauf le marché permettant d'acheter des cartes

L'ensemble du back a été codé

Le lien entre le back et le front est partiel. Un utilisateur est connecté par défaut et il lui est possible de vendre ses cartes.
